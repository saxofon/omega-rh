#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <locale.h>
#include <pthread.h>
#include <sys/types.h>
#include <libcrh.h>

static void dump_crh_info_title(void)
{
	printf("Interface            Temperature(°C) Humidity(%RH)\n");
}

static void dump_crh_info_data(struct s_crh *crh)
{
	printf("%-20s %-15.1f %-10.1f\r",
		crh->interface, crh->temperature, crh->humidity);
}

int main(int argc, char **argv)
{
	struct s_crh *crh;

	setlocale(LC_ALL, "");

	crh = libcrh_poll("/dev/ttyUSB0");

	dump_crh_info_title();
	while(1) {
		sleep(1);
		if (!crh || crh->error) {
			printf("Probably sensor device trouble. Sensor available at %s?\n", crh->interface);
			break;
		}
		dump_crh_info_data(crh);
		fflush(stdout);
	}

	if (crh)
		libcrh_poll_stop(crh);
}
