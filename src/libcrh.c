#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <termios.h>
#include <libcrh.h>

static void *libcrh_poll_if(void *arg)
{
	struct s_crh *crh = (struct s_crh*)arg;
	struct termios tp;
	int status;
	size_t n;
	char *line;

	crh->f = fopen(crh->interface, "r+");
	if (!crh->f) {
		crh->error = -ENODEV;
		pthread_exit(&crh->error);
	}

	if (tcgetattr(fileno(crh->f), &tp) == -1) {
		crh->error = errno;
		pthread_exit(&crh->error);
	}
	tp.c_lflag &= ~ECHO;
	if (tcsetattr(fileno(crh->f), TCSAFLUSH, &tp) == -1) {
		crh->error = errno;
		pthread_exit(&crh->error);
	}

	while (1) {
		status = fprintf(crh->f, "c\r\n");
		fflush(crh->f);
		line = NULL;
		n=0;
		n = getline(&line, &n, crh->f);
		status = sscanf(line, ">%f C", &crh->temperature);
		n = getline(&line, &n, crh->f);
		free(line);

		status = fprintf(crh->f, "h\r\n");
		fflush(crh->f);
		line = NULL;
		n=0;
		n = getline(&line, &n, crh->f);
		status = sscanf(line, ">%f %%RH", &crh->humidity);
		n = getline(&line, &n, crh->f);
		free(line);

		sleep(1);
	}

	fclose(crh->f);
	crh->error = 0;
	pthread_exit(&crh->error);
}

struct s_crh *libcrh_poll(const char *interface)
{
	struct s_crh *crh;

	crh = (struct s_crh*)malloc(sizeof(struct s_crh));
	if (!crh) {
		printf("not room for more crh structures!\n");
		exit(-ENOMEM);
	}
	crh->interface = strdup(interface);
	pthread_create(&crh->tid, NULL, &libcrh_poll_if, (void*)crh);

	return crh;
}

int libcrh_poll_stop(const struct s_crh *crh)
{
	return pthread_cancel(crh->tid);
}
