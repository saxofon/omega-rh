#ifndef __LIBCRH_H__
#define __LIBCRH_H__

#define LIBCRH_MAX_INTERFACES 16

struct s_crh {
	char *interface;
	pthread_t tid;
	int error;
	FILE *f;
	float temperature;
	float humidity;
};

struct s_crh *libcrh_poll(const char *interface);
int libcrh_poll_stop(const struct s_crh *crh);

#endif
